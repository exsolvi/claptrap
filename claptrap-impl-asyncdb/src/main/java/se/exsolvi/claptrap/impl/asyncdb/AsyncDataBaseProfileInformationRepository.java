package se.exsolvi.claptrap.impl.asyncdb;

import java.lang.reflect.Method;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.slf4j.Logger;

@Stateful
public class AsyncDataBaseProfileInformationRepository {

    @Inject
    private Logger log;
  
    @EJB
    private DatabaseWriter databaseWriter;
    
    @Asynchronous
    public void put(Object target, Method calledMethod, long startTime, long elapsedTime) {
        databaseWriter.put(target, calledMethod, startTime, elapsedTime);
    }

    public void clear(String methodName) {
        log.debug("Clearing profile information for {}", methodName);
        throw new RuntimeException("Not yet implemented");
    }

    public void clear() {
        log.debug("Clearing profile database");
        throw new RuntimeException("Not yet implemented");
    }



}
