package se.exsolvi.claptrap.impl.asyncdb;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import se.exsolvi.claptrap.impl.asyncdb.entitymodel.MethodCallEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledClassEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledMethodEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledPackageEntity;

@Singleton
@Local(value=CachingEntityHandler.class)
public class CachingEntityHandler implements EntityHandler {

    @PersistenceContext(name = "ProfileInformationDS")
    private EntityManager em;

    private Map<String, ProfiledMethodEntity> methodCache = new ConcurrentHashMap<>();

    private Map<String, ProfiledClassEntity> classCache = new ConcurrentHashMap<>();

    private Map<String, ProfiledPackageEntity> packageCache = new ConcurrentHashMap<>();

    @Override
    public ProfiledMethodEntity getProfiledMethod(String name) {
        ProfiledMethodEntity profiledMethod = methodCache.get(name);
        if (profiledMethod == null) {
            profiledMethod = findMethodByName(name);
            if (profiledMethod != null) {
                methodCache.put(profiledMethod.getName(), profiledMethod);
            }
        }
        return profiledMethod;
    }

    @Override
    public void putProfiledMethod(ProfiledMethodEntity profiledMethod) {
        methodCache.put(profiledMethod.getName(), profiledMethod);
        em.persist(profiledMethod);
    }

    @Override
    public ProfiledClassEntity getProfiledClass(String name) {
        ProfiledClassEntity profiledClass = classCache.get(name);
        if (profiledClass == null) {
            profiledClass = findClassByName(name);
            if (profiledClass != null) {
                classCache.put(profiledClass.getName(), profiledClass);
            }
        }
        return profiledClass;
    }

    @Override
    public void putProfiledClass(ProfiledClassEntity profiledClass) {
        classCache.put(profiledClass.getName(), profiledClass);
        em.persist(profiledClass);
    }

    @Override
    public ProfiledPackageEntity getProfiledPackage(String name) {
        ProfiledPackageEntity profiledPackage = packageCache.get(name);
        if (profiledPackage == null) {
            profiledPackage = findPackageByName(name);
            if (profiledPackage != null) {
                packageCache.put(profiledPackage.getName(), profiledPackage);
            }
        }
        return profiledPackage;
    }

    @Override
    public void putProfiledPackage(ProfiledPackageEntity profiledPackage) {
        packageCache.put(profiledPackage.getName(), profiledPackage);
        em.persist(profiledPackage);
    }

    @Override
    public ProfiledMethodEntity findMethodByName(String name) {
        ProfiledMethodEntity profiledMethod = null;
        TypedQuery<ProfiledMethodEntity> query = em.createNamedQuery("findMethodByName", ProfiledMethodEntity.class);
        query.setParameter("name", name);
        try {
            profiledMethod = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledMethod;
    }

    @Override
    public ProfiledClassEntity findClassByName(String name) {
        ProfiledClassEntity profiledClass = null;
        TypedQuery<ProfiledClassEntity> query = em.createNamedQuery("findClassByName", ProfiledClassEntity.class);
        query.setParameter("name", name);
        try {
            profiledClass = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledClass;
    }

    @Override
    public ProfiledPackageEntity findPackageByName(String name) {
        ProfiledPackageEntity profiledPackage = null;
        TypedQuery<ProfiledPackageEntity> query = em.createNamedQuery("findPackageByName", ProfiledPackageEntity.class);
        query.setParameter("name", name);
        try {
            profiledPackage = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledPackage;
    }

    @Override
    public void putMethodCallEntity(MethodCallEntity methodCallEntity) {
        em.persist(methodCallEntity);
    }

}
