package se.exsolvi.claptrap.impl.asyncdb;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import se.exsolvi.claptrap.api.Profiled;

@Profiled
@Interceptor
public class DatabaseProfiler implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private AsyncDataBaseProfileInformationRepository profileInformationRepository;

    @AroundInvoke
    public Object profile(InvocationContext context) throws Exception {
        final long startTime = System.currentTimeMillis();
        Object o = context.proceed();
        final long elapsedTime = System.currentTimeMillis() - startTime;
        profileInformationRepository.put(context.getTarget(), context.getMethod(), startTime, elapsedTime);
        return o;
    }
}
