package se.exsolvi.claptrap.impl.asyncdb;

import java.lang.reflect.Method;

import javax.ejb.EJB;
import javax.ejb.Stateful;

import se.exsolvi.claptrap.impl.asyncdb.entitymodel.MethodCallEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledClassEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledMethodEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledPackageEntity;
import se.exsolvi.claptrap.model.ProfiledClass;

@Stateful
public class DatabaseWriter {

    @EJB
    private CachingEntityHandler entityHandler;

    public void put(Object target, Method calledMethod, long startTime, long elapsedTime) {
        ProfiledMethodEntity profiledMethod = findOrCreateProfiledMethod(calledMethod);
        ProfiledClass callingClass = findOrCreateProfiledClass(target.getClass());
        MethodCallEntity methodCall = new MethodCallEntity(callingClass, profiledMethod, startTime, elapsedTime);
        entityHandler.putMethodCallEntity(methodCall);
    }

    private ProfiledMethodEntity findOrCreateProfiledMethod(Method callingMethod) {
        ProfiledMethodEntity profiledMethod = entityHandler.getProfiledMethod(callingMethod.getName());
        if (profiledMethod == null) {
            ProfiledClassEntity profiledClass = findOrCreateProfiledClass(callingMethod.getDeclaringClass());
            profiledMethod = new ProfiledMethodEntity(callingMethod.getName(), profiledClass);
            entityHandler.putProfiledMethod(profiledMethod);
        }
        return profiledMethod;
    }

    private ProfiledClassEntity findOrCreateProfiledClass(Class<?> callingClass) {
        ProfiledClassEntity profiledClass = entityHandler.getProfiledClass(callingClass.getSimpleName());
        if (profiledClass == null) {
            ProfiledPackageEntity profiledPackage = findOrCreateProfiledPackage(callingClass.getPackage());
            profiledClass = new ProfiledClassEntity(callingClass.getSimpleName(), profiledPackage);
            entityHandler.putProfiledClass(profiledClass);
        }
        return profiledClass;
    }

    private ProfiledPackageEntity findOrCreateProfiledPackage(Package callingClass) {
        ProfiledPackageEntity profiledPackage = entityHandler.getProfiledPackage(callingClass.getName());
        if (profiledPackage == null) {
            profiledPackage = new ProfiledPackageEntity(callingClass.getName());
            entityHandler.putProfiledPackage(profiledPackage);
        }
        return profiledPackage;
    }
}
