package se.exsolvi.claptrap.impl.asyncdb;

import se.exsolvi.claptrap.impl.asyncdb.entitymodel.MethodCallEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledClassEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledMethodEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledPackageEntity;

public interface EntityHandler {

    public ProfiledMethodEntity getProfiledMethod(String name);

    public void putProfiledMethod(ProfiledMethodEntity profiledMethod);

    public ProfiledClassEntity getProfiledClass(String name);

    public void putProfiledClass(ProfiledClassEntity profiledClass);

    public ProfiledPackageEntity getProfiledPackage(String name);

    public void putProfiledPackage(ProfiledPackageEntity profiledPackage);

    public ProfiledMethodEntity findMethodByName(String name);

    public ProfiledClassEntity findClassByName(String name);

    public ProfiledPackageEntity findPackageByName(String name);

    public void putMethodCallEntity(MethodCallEntity methodCallEntity);
    
}