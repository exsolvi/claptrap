package se.exsolvi.claptrap.impl.asyncdb;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import se.exsolvi.claptrap.impl.asyncdb.entitymodel.MethodCallEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledClassEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledMethodEntity;
import se.exsolvi.claptrap.impl.asyncdb.entitymodel.ProfiledPackageEntity;

@Singleton
@Local(value = NonCachingEntityHandler.class)
public class NonCachingEntityHandler implements EntityHandler {

    @PersistenceContext(name = "ProfileInformationDS")
    private EntityManager em;

    @Override
    public ProfiledMethodEntity getProfiledMethod(String name) {
        return findMethodByName(name);
    }

    @Override
    public void putProfiledMethod(ProfiledMethodEntity profiledMethod) {
        em.persist(profiledMethod);
    }

    @Override
    public ProfiledClassEntity getProfiledClass(String name) {
        return findClassByName(name);
    }

    @Override
    public void putProfiledClass(ProfiledClassEntity profiledClass) {
        em.persist(profiledClass);
    }

    @Override
    public ProfiledPackageEntity getProfiledPackage(String name) {
        return findPackageByName(name);
    }

    @Override
    public void putProfiledPackage(ProfiledPackageEntity profiledPackage) {
        em.persist(profiledPackage);
    }

    @Override
    public ProfiledMethodEntity findMethodByName(String name) {
        ProfiledMethodEntity profiledMethod = null;
        TypedQuery<ProfiledMethodEntity> query = em.createNamedQuery("findMethodByName", ProfiledMethodEntity.class);
        query.setParameter("name", name);
        try {
            profiledMethod = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledMethod;
    }

    @Override
    public ProfiledClassEntity findClassByName(String name) {
        ProfiledClassEntity profiledClass = null;
        TypedQuery<ProfiledClassEntity> query = em.createNamedQuery("findClassByName", ProfiledClassEntity.class);
        query.setParameter("name", name);
        try {
            profiledClass = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledClass;
    }

    @Override
    public ProfiledPackageEntity findPackageByName(String name) {
        ProfiledPackageEntity profiledPackage = null;
        TypedQuery<ProfiledPackageEntity> query = em.createNamedQuery("findPackageByName", ProfiledPackageEntity.class);
        query.setParameter("name", name);
        try {
            profiledPackage = query.getSingleResult();
        } catch (PersistenceException pe) {
            // Ignore exceptions.
        }
        return profiledPackage;
    }

    @Override
    public void putMethodCallEntity(MethodCallEntity methodCallEntity) {
        em.persist(methodCallEntity);
    }

}
