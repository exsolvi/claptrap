package se.exsolvi.claptrap.impl.asyncdb.entitymodel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import se.exsolvi.claptrap.model.MethodCall;
import se.exsolvi.claptrap.model.ProfiledClass;
import se.exsolvi.claptrap.model.ProfiledMethod;

@Entity
@Table(name = "METRICS_MethodCall")
public class MethodCallEntity extends BaseEntity implements MethodCall {

    @JoinColumn(name = "method", nullable = false)
    @ManyToOne(targetEntity = ProfiledMethodEntity.class)
    private ProfiledMethod profiledMethod;

    @JoinColumn(name = "class", nullable = false)
    @ManyToOne(targetEntity = ProfiledClassEntity.class)
    private ProfiledClass targetClass;

    @Column(name = "callSignature")
    private String callSignature;

    @Column(name = "callTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date callTime;

    @Column(name = "durationInMilliSeconds")
    private long durationInMilliSeconds;

    public MethodCallEntity() {
        // JPA Entities needs a no-args constructor
    }

    public MethodCallEntity(ProfiledClass targetClass, ProfiledMethod profiledMethod, long callTime,
            long durationInMilliSeconds) {
        this.targetClass = targetClass;
        this.profiledMethod = profiledMethod;
        this.callSignature = targetClass.getProfiledPackage().getName() + "." + targetClass.getName() + ":"
                + profiledMethod.getName();
        this.callTime = new Date(callTime);
        this.durationInMilliSeconds = durationInMilliSeconds;
    }

    @Override
    public ProfiledClass getTargetClass() {
        return targetClass;
    }

    @Override
    public ProfiledMethod getProfiledMethod() {
        return profiledMethod;
    }

    @Override
    public String getCallSignature() {
        return callSignature;
    }

    @Override
    public long getCalltime() {
        return callTime.getTime();
    }

    @Override
    public long getDurationInMilliSeconds() {
        return durationInMilliSeconds;
    }
}