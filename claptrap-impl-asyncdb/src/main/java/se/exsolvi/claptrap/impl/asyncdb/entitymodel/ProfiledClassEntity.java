package se.exsolvi.claptrap.impl.asyncdb.entitymodel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.exsolvi.claptrap.model.ProfiledClass;
import se.exsolvi.claptrap.model.ProfiledPackage;

@Entity
@NamedQuery(name = "findClassByName", query = "SELECT c FROM ProfiledClassEntity c WHERE c.name = :name")
@Table(name = "METRICS_Class", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "package" }))
public class ProfiledClassEntity extends BaseEntity implements ProfiledClass {

    private String name;

    @ManyToOne(targetEntity = ProfiledPackageEntity.class)
    @JoinColumn(name = "package", nullable = false)
    private ProfiledPackage profiledPackage;

    public ProfiledClassEntity() {
        // JPA Entities needs a no-args constructor
    }

    public ProfiledClassEntity(String name, ProfiledPackageEntity profiledPackage) {
        this.name = name;
        this.profiledPackage = profiledPackage;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ProfiledPackage getProfiledPackage() {
        return profiledPackage;
    }

}
