package se.exsolvi.claptrap.impl.asyncdb.entitymodel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.exsolvi.claptrap.model.ProfiledClass;
import se.exsolvi.claptrap.model.ProfiledMethod;

@Entity
@NamedQuery(name = "findMethodByName", query = "SELECT m FROM ProfiledMethodEntity m WHERE m.name = :name")
@Table(name = "METRICS_Method", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "class" }))
public class ProfiledMethodEntity extends BaseEntity implements ProfiledMethod {

    private String name;

    @JoinColumn(name = "class", nullable = false)
    @ManyToOne(targetEntity = ProfiledClassEntity.class)
    private ProfiledClass profiledClass;

    public ProfiledMethodEntity() {
        // JPA Entities needs a no-args constructor
    }

    public ProfiledMethodEntity(String name, ProfiledClass profiledClass) {
        this.name = name;
        this.profiledClass = profiledClass;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ProfiledClass getProfiledClass() {
        return profiledClass;
    }
}
