package se.exsolvi.claptrap.impl.asyncdb.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import se.exsolvi.claptrap.model.ProfiledPackage;

@Entity
@NamedQuery(name = "findPackageByName", query = "SELECT p FROM ProfiledPackageEntity p WHERE p.name = :name")
@Table(name = "METRICS_Package")
public class ProfiledPackageEntity extends BaseEntity implements ProfiledPackage {

    @Column(unique = true)
    private String name;

    public ProfiledPackageEntity() {
        // JPA Entities needs a no-args constructor
    }

    public ProfiledPackageEntity(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

}
