package se.exsolvi.claptrap.impl.memory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class ProfileInformation {
    
    @Inject 
    private Logger log;
    
    private static final int MAX_SIZE = 10;
    private Map<String, LinkedList<Long>> methodTimes = new HashMap<>();

    public void put(String methodSignature, long elapsedTime) {
        LinkedList<Long> invokationtimes = methodTimes.get(methodSignature);
        if (invokationtimes == null) {
            invokationtimes = new LinkedList<>();
            methodTimes.put(methodSignature, invokationtimes);
        }

        invokationtimes.push(elapsedTime);

        if (invokationtimes.size() > MAX_SIZE) {
            invokationtimes.removeLast();
        }
    }

    public Set<String> getProfiledMethodNames() {
        return methodTimes.keySet();
    }

    public long averageInvokationTime(String methodName) {
        return average(methodTimes.get(methodName));
    }

    private long average(List<Long> list) {
        Long sum = 0L;
        for (Long value : list) {
            sum += value;
        }
        return sum / list.size();
    }

    public void clear(String methodName) {
        methodTimes.remove(methodName);
    }

    public void clear() {
        log.debug("Profiling buffer cleared");
        methodTimes.clear();
    }

}
