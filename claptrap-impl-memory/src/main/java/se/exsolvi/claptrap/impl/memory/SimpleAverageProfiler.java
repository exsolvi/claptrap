package se.exsolvi.claptrap.impl.memory;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import se.exsolvi.claptrap.api.Profiled;

@Profiled
@Interceptor
public class SimpleAverageProfiler {

    @Inject
    private ProfileInformation profileInformation;

    @AroundInvoke
    public Object profile(InvocationContext context) throws Exception {

        final String methodSignature = context.getMethod().toString();

        final long startTime = System.currentTimeMillis();
        Object o = context.proceed();
        final long elapsedTime = System.currentTimeMillis() - startTime;

        profileInformation.put(methodSignature, elapsedTime);
        return o;
    }
}
