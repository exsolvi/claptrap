package se.exsolvi.claptrap.impl.memory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.exsolvi.util.cdi.CDITestHelper;

//CHECKSTYLE:OFF

public class ProfileInformationTest {

    private static final String METHOD_NAME = "package.test";

    private static Logger log = LoggerFactory.getLogger(ProfileInformation.class);
    
    @Test
    public void testPut() {
        ProfileInformation profileInformation = getClassUnderTest();
        profileInformation.put(METHOD_NAME, 1L);
        assertEquals(1, profileInformation.averageInvokationTime(METHOD_NAME));
    }

    @Test
    public void testAverageInvokationTime() {
        ProfileInformation profileInformation = getClassUnderTest();
        profileInformation.put(METHOD_NAME, 1L);
        profileInformation.put(METHOD_NAME, 2L);
        profileInformation.put(METHOD_NAME, 3L);
        profileInformation.put(METHOD_NAME, 4L);
        profileInformation.put(METHOD_NAME, 5L);
        assertEquals(3, profileInformation.averageInvokationTime(METHOD_NAME));
    }

    @Test
    public void testClear() {
        ProfileInformation profileInformation = getClassUnderTest();
        profileInformation.put(METHOD_NAME + 1, 1L);
        profileInformation.put(METHOD_NAME + 1, 2L);
        profileInformation.put(METHOD_NAME + 2, 3L);
        profileInformation.clear();
        assertEquals(0, profileInformation.getProfiledMethodNames().size());
    }

    @Test
    public void testClearMethodName() {
        ProfileInformation profileInformation = getClassUnderTest();
        profileInformation.put(METHOD_NAME + 1, 1L);
        profileInformation.put(METHOD_NAME + 1, 2L);
        profileInformation.put(METHOD_NAME + 2, 3L);
        profileInformation.put(METHOD_NAME + 3, 4L);
        profileInformation.put(METHOD_NAME + 4, 5L);
        profileInformation.clear(METHOD_NAME + 3);
        assertEquals(3, profileInformation.getProfiledMethodNames().size());
    }

    @Test
    public void testMethodNameSorting() {
        ProfileInformation profileInformation = getClassUnderTest();
        profileInformation.put(METHOD_NAME + 1, 1L);
        profileInformation.put(METHOD_NAME + 2, 2L);
        profileInformation.put(METHOD_NAME + 2, 4L);
        profileInformation.put(METHOD_NAME + 3, 3L);
        profileInformation.put(METHOD_NAME + 3, 5L);
        profileInformation.put(METHOD_NAME + 3, 7L);
        assertEquals(3, profileInformation.getProfiledMethodNames().size());
        assertEquals(1, profileInformation.averageInvokationTime(METHOD_NAME + 1));
        assertEquals(3, profileInformation.averageInvokationTime(METHOD_NAME + 2));
        assertEquals(5, profileInformation.averageInvokationTime(METHOD_NAME + 3));
    }
    
    private ProfileInformation getClassUnderTest() {
        ProfileInformation profileInformation = new ProfileInformation();
        CDITestHelper.reflectInjectField(profileInformation, "log", log);
        return profileInformation;
    }
}

//CHECKSTYLE:ON
