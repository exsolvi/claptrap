package se.exsolvi.claptrap.model;

public interface MethodCall {

    public ProfiledClass getTargetClass();

    public ProfiledMethod getProfiledMethod();

    public String getCallSignature();

    public long getCalltime();

    public long getDurationInMilliSeconds();

}