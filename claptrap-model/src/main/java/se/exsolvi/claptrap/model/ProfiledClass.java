package se.exsolvi.claptrap.model;

public interface ProfiledClass {

    public String getName();

    public ProfiledPackage getProfiledPackage();

}