package se.exsolvi.claptrap.model;

public interface ProfiledMethod {

    public String getName();

    public ProfiledClass getProfiledClass();

}